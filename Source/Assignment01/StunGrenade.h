// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Grenade.h"
#include "StunGrenade.generated.h"

/**
 * 
 */
UCLASS()
class ASSIGNMENT01_API AStunGrenade : public AGrenade
{
	GENERATED_BODY()

public:
    /** Projectile class to spawn */
    UPROPERTY(EditAnywhere, Category = Projectile)
    TSubclassOf<class AGrenadeProjectile> ProjectileClass;
	
    UFUNCTION(BlueprintCallable, Category = "Interact")
         void ThrowSelf();
    
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interact")
        void Interact(AActor* OtherActor);

    virtual void Interact_Implementation(AActor* OtherActor) override;

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interact")
        FString ViewAction(AActor* OtherActor);

    virtual FString ViewAction_Implementation(AActor* OtherActor) override;

    void BeginPlay() override;
};
