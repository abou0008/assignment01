// Fill out your copyright notice in the Description page of Project Settings.


#include "LockedDoor.h"
#include "Assignment01Character.h"
#include "Kismet/GameplayStatics.h"

void ALockedDoor::Interact_Implementation(AActor* OtherActor)
{
    if(Locked)
    {
        if(OtherActor->GetClass()->StaticClass() == UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)->GetClass()->StaticClass())
        {
            AAssignment01Character* Ass01Character = Cast<AAssignment01Character>(OtherActor);
            if(Ass01Character->HandItem)
            {
                if(LinkedKey == Ass01Character->HandItem)
                {
                    Locked = false;
                    Ass01Character->DropItem();
                    if (MatInstance != nullptr)
                        MatInstance->SetScalarParameterValue("Emission", 1.5);
                }
            }
        }
    }
    else
        Open = !Open;
}

FString ALockedDoor::ViewAction_Implementation(AActor* OtherActor)
{
    if(Locked)
    {
        if(OtherActor->GetClass()->StaticClass() == UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)->GetClass()->StaticClass())
        {
            AAssignment01Character* Ass01Character = Cast<AAssignment01Character>(OtherActor);
            if(Ass01Character->HandItem)
            {
                if(LinkedKey == Ass01Character->HandItem)
                     return "Locked Door: Press E to unlock";
                return "Locked Door: That item will not open this door!";
            }
        }
        return "Locked Door: Press E while holding a key to unlock";
    }
    return "Unlocked Door: Press E to open";
}
