// Fill out your copyright notice in the Description page of Project Settings.

#include "Assignment01Character.h"
#include "HealthPack.h"
#include "Kismet/GameplayStatics.h"

void AHealthPack::BeginPlay()
{
    Super::BeginPlay();
    InspectMessage = "Health Pack: Heals the player 100 health";
    
    Ass01Character = Cast<AAssignment01Character>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
}

void AHealthPack::Interact_Implementation(AActor* OtherActor)
{
    if(OtherActor->GetClass()->StaticClass() == Ass01Character->GetClass()->StaticClass())
    {
        Ass01Character->AddHealth(100.f);

        this->Destroy();
    }
}

FString AHealthPack::ViewAction_Implementation(AActor* OtherActor)
{
    return InspectMessage;
}