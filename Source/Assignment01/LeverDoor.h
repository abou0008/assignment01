// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Door.h"
#include "Lever.h"
#include "LeverDoor.generated.h"

/**
 * 
 */
UCLASS()
class ASSIGNMENT01_API ALeverDoor : public ADoor
{
	GENERATED_BODY()

protected:
	int BaseZ;

	virtual void BeginPlay() override;

private:
	UPROPERTY(EditAnywhere, Category = "Interact")
	ALever* LinkedLever;
	
public:
	ALeverDoor();
	
	virtual void Tick(float DeltaTime) override;
	
	virtual void Interact_Implementation(AActor* OtherActor) override;

	virtual FString ViewAction_Implementation(AActor* OtherActor) override;
};
