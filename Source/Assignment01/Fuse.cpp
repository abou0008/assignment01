// Fill out your copyright notice in the Description page of Project Settings.

#include "Assignment01Character.h"
#include "Fuse.h"
#include "Kismet/GameplayStatics.h"

void AFuse::BeginPlay()
{
    Super::BeginPlay();
    InspectMessage = "A Fuse: Used in a fuse box to reset it.";

    Ass01Character = Cast<AAssignment01Character>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
}

void AFuse::Interact_Implementation(AActor* OtherActor)
{
    if(OtherActor->GetClass()->StaticClass() == Ass01Character->GetClass()->StaticClass())
    {
        if(Ass01Character->HandItem == nullptr)
        {
            Ass01Character->HoldItem(this, "FuseComp");
            SetActorHiddenInGame(true);
            SetActorEnableCollision(false);
        }
    }
}

FString AFuse::ViewAction_Implementation(AActor* OtherActor)
{
    if(OtherActor->GetClass()->StaticClass() == Ass01Character->GetClass()->StaticClass())
    {
        if(Ass01Character->HandItem != nullptr)
            InspectMessage = "You are already holding an item!";
        else
            InspectMessage = "A Fuse: Used in a fuse box to reset it.";
    }
	
    return InspectMessage;
}