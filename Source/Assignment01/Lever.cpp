// Fill out your copyright notice in the Description page of Project Settings.


#include "Lever.h"

// Sets default values
ALever::ALever()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Root"));
	RootComponent = Root;

	LeverBase = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("LeverBase"));
	LeverBase->SetupAttachment(RootComponent);
	
	LeverArm = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("LeverArm"));
	LeverArm->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ALever::BeginPlay()
{
	Super::BeginPlay();

	Material = LeverArm->GetMaterial(0);
	MatInstance = LeverArm->CreateDynamicMaterialInstance(0, Material);		
}

// Called every frame
void ALever::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FVector DoorTarget;
	FRotator ArmTarget = LeverArm->GetRelativeRotation();
	FLinearColor LeverColour;
	
	if(OnState)
	{
		ArmTarget.Pitch = OnAngle;
		LeverColour = FLinearColor(0.0857, 1.0, 0.0135);
	}
	else
	{
		ArmTarget.Pitch = -OnAngle;
		LeverColour = FLinearColor(1.0, 0.021, 0.0174);
	}

	LeverArm->SetRelativeRotation(FMath::Lerp(LeverArm->GetRelativeRotation(), ArmTarget, 0.3f));
	MatInstance->SetVectorParameterValue("Colour", FMath::Lerp(MatInstance->K2_GetVectorParameterValue("Colour"), LeverColour, 0.3f));
}

void ALever::Interact_Implementation(AActor* OtherActor)
{
	OnState = !OnState;
}

FString ALever::ViewAction_Implementation(AActor* OtherActor)
{
	return "Lever: Press E to toggle";
}

bool ALever::IsOn()
{
	return OnState;
}
