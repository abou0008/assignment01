// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactable.h"
#include "GameFramework/Actor.h"
#include "Door.generated.h"

UCLASS()
class ASSIGNMENT01_API ADoor : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADoor();
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interact")
	class UStaticMeshComponent* BaseMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interact")
	class USceneComponent* SceneComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interact")
	class UStaticMeshComponent* DoorMesh;

	UPROPERTY(EditAnywhere, Category = "Interact")
	UMaterialInterface* Material;

	UMaterialInstanceDynamic* MatInstance;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interact")
		bool Open = false;
	float StartingRotation;
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interact")
    void Interact(AActor* OtherActor);
	
	virtual void Interact_Implementation(AActor* OtherActor) override;
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interact")
    FString ViewAction(AActor* OtherActor);

	virtual FString ViewAction_Implementation(AActor* OtherActor) override;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
