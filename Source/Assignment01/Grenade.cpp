// Fill out your copyright notice in the Description page of Project Settings.

#include "Assignment01Character.h"
#include "Kismet/GameplayStatics.h"
#include "Grenade.h"

#include "GrenadeProjectile.h"

void AGrenade::BeginPlay()
{
    Super::BeginPlay();
    InspectMessage = "A Grenade: Press E to pick up.";

    Ass01Character = Cast<AAssignment01Character>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
}

void AGrenade::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
}
