// Fill out your copyright notice in the Description page of Project Settings.


#include "LeverDoor.h"

ALeverDoor::ALeverDoor()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;
}


void ALeverDoor::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    
    FVector DoorTarget = BaseMesh->GetComponentLocation();

    if (this->GetClass()->GetName() == "BP_LeverDoor_C")
    {
        if (LinkedLever)
        {
            if (LinkedLever->IsOn())
            {
                DoorTarget.Z = BaseZ + 250.5f;
            }
            else
            {
                DoorTarget.Z = BaseZ;
            }
        }
    }
    
    BaseMesh->SetRelativeLocation(FMath::Lerp(BaseMesh->GetComponentLocation(), DoorTarget, 0.05f));
}

void ALeverDoor::BeginPlay()
{
    Super::BeginPlay();
    
    BaseZ = BaseMesh->GetComponentLocation().Z;
}

void ALeverDoor::Interact_Implementation(AActor* OtherActor)
{
    return;
}

FString ALeverDoor::ViewAction_Implementation(AActor* OtherActor)
{
    return "Lever Activated Door: Find the Lever that opens me!";
}
