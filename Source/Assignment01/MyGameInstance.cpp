// Fill out your copyright notice in the Description page of Project Settings.

#include "MyGameInstance.h"
#include "Kismet/GameplayStatics.h"

UMyGameInstance::UMyGameInstance()
{
     GameState = Cinematic;
}

void UMyGameInstance::SetGameState(int NewGameState)
{
     GameState = (GameStates) NewGameState;
     OnGameStateChange.Broadcast(GameState);
}

int UMyGameInstance::GetGameState()
{
     return GameState;
}

void UMyGameInstance::PauseGame()
{
     if(GetGameState() == Playing)
     {
          SetGameState(Paused);
          UGameplayStatics::SetGlobalTimeDilation(GetWorld(), 0);
     }
     else if (GetGameState() == Paused)
     {
          SetGameState(Playing);
          UGameplayStatics::SetGlobalTimeDilation(GetWorld(), 1);
     }
     else
     {
          UGameplayStatics::SetGlobalTimeDilation(GetWorld(), 1);
     }
}

void UMyGameInstance::ResetGame()
{
     // Found at https://answers.unrealengine.com/questions/253156/how-to-get-current-running-levels-name.html
     UGameplayStatics::OpenLevel(GetWorld(), FName(*GetWorld()->GetName()), false);
     UGameplayStatics::SetGlobalTimeDilation(GetWorld(), 1);

     if (GetGameState() == 0)
          SetGameState(StartMenu);
     else if (GetGameState() == 1)
          SetGameState(Playing);
     else if (GetGameState() == 7)
          SetGameState(StartMenu);
}