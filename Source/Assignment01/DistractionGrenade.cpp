// Fill out your copyright notice in the Description page of Project Settings.

#include "Kismet/GameplayStatics.h"
#include "Assignment01Character.h"
#include "GrenadeProjectile.h"
#include "DistractionGrenade.h"

void ADistractionGrenade::BeginPlay()
{
    Super::BeginPlay();
    InspectMessage = "A Distraction Grenade: Press E to pick up, and Left-Click to throw.";

    Ass01Character = Cast<AAssignment01Character>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
}

void ADistractionGrenade::ThrowSelf()
{
    if (ProjectileClass)
    {
        UWorld* const World = GetWorld();
        FVector SpawnLocation = Ass01Character->FP_MuzzleLocation->GetComponentLocation();
        SpawnLocation.Z += 50;
        FRotator SpawnRotation = Ass01Character->GetControlRotation();
        SpawnRotation.Pitch += 5;

        FActorSpawnParameters ActorSpawnParams;
        ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

        World->SpawnActor<AGrenadeProjectile>(ProjectileClass, SpawnLocation, SpawnRotation, ActorSpawnParams);
    }
    else
    {
        UE_LOG(LogTemp, Warning, TEXT("PROJECTILECLASS NOT SET"));
    }
}

void ADistractionGrenade::Interact_Implementation(AActor* OtherActor)
{
    if(OtherActor->GetClass()->StaticClass() == Ass01Character->GetClass()->StaticClass())
    {
        if (!inHand)
        {
            if(Ass01Character->HandItem == nullptr)
            {
                Ass01Character->HoldItem(this, "DistractionGrenadeComp");
                SetActorHiddenInGame(true);
                SetActorEnableCollision(false);
                inHand = true;
            }
        }
        else
        {
            ThrowSelf();
            Ass01Character->DropItem();
            Destroy();
        }
    }
}

FString ADistractionGrenade::ViewAction_Implementation(AActor* OtherActor)
{
    if(OtherActor->GetClass()->StaticClass() == Ass01Character->GetClass()->StaticClass())
    {
        if (!inHand)
        {
            if(Ass01Character->HandItem != nullptr)
                InspectMessage = "You are already holding an item!";
            else
                InspectMessage = "A Distraction Grenade: Press E to pick up, and Left-Click to throw.";
        }
        else
        {
            InspectMessage = "";
        }
    }
	
    return InspectMessage;
}
