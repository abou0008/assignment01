// Fill out your copyright notice in the Description page of Project Settings.

#include "EnemyCharacter.h"
#include "EnemyAIController.h"

AEnemyAIController::AEnemyAIController()
{
    PrimaryActorTick.bCanEverTick = true;

    SightConfiguration = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("Sight Configuration"));
    SetPerceptionComponent(*CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("Perception Component")));

    SightConfiguration->SightRadius = SightRadius;
    SightConfiguration->LoseSightRadius = LoseSightRadius;
    SightConfiguration->PeripheralVisionAngleDegrees = FieldOfView;
    SightConfiguration->SetMaxAge(SightAge);

    SightConfiguration->DetectionByAffiliation.bDetectEnemies = true;
    SightConfiguration->DetectionByAffiliation.bDetectFriendlies = true;
    SightConfiguration->DetectionByAffiliation.bDetectNeutrals = true;

    GetPerceptionComponent()->SetDominantSense(*SightConfiguration->GetSenseImplementation());
    GetPerceptionComponent()->ConfigureSense(*SightConfiguration);
    GetPerceptionComponent()->OnTargetPerceptionUpdated.AddDynamic(this, &AEnemyAIController::OnSensesUpdated);

    TargetPlayer = nullptr;
}

void AEnemyAIController::BeginPlay()
{
    Super::BeginPlay();

    NavigationSystem = Cast<UNavigationSystemV1>(GetWorld()->GetNavigationSystem());
    if (NavigationSystem)
        UE_LOG(LogTemp, Warning, TEXT("Nav System Found"));

    if (!AIBlackboard)
        return;
    if (!ensure(BehaviourTree))
        return;

    UseBlackboard(AIBlackboard, BlackboardComponent);
    RunBehaviorTree(BehaviourTree);
}

void AEnemyAIController::OnPossess(APawn* InPawn)
{
    Super::OnPossess(InPawn);
}

void AEnemyAIController::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

    bool IsStunned = BlackboardComponent->GetValueAsBool("IsStunned");
    bool IsDistracted = BlackboardComponent->GetValueAsBool("IsDistracted");
    
    if (TargetPlayer && !IsStunned && !IsDistracted)
    {
        BlackboardComponent->SetValueAsVector("PlayerPosition", TargetPlayer->GetActorLocation());

        float Distance = FVector::Distance(TargetPlayer->GetActorLocation(), GetPawn()->GetActorLocation());
        if (Distance <= 200)
        {
            TargetPlayer->AddHealth(-10 * DeltaSeconds);
        }
    }
}

FRotator AEnemyAIController::GetControlRotation() const
{
    if (GetPawn())
    {
        return FRotator(0.f, GetPawn()->GetActorRotation().Yaw, 0.f);
    }

    return FRotator(0, 0, 0);
}

void AEnemyAIController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result)
{
    UE_LOG(LogTemp, Warning, TEXT("Reached Location"));
}

void AEnemyAIController::OnSensesUpdated(AActor* UpdatedActor, FAIStimulus Stimulus)
{
    UE_LOG(LogTemp, Warning, TEXT("Sensed"));
    APawn* TemporaryPawn = Cast<APawn>(UpdatedActor);
    if (TemporaryPawn && TemporaryPawn->IsPlayerControlled())
    {
        if (Stimulus.WasSuccessfullySensed())
        {
            UE_LOG(LogTemp, Warning, TEXT("Set Actor Location"));
            TargetPlayer = Cast<AAssignment01Character>(UpdatedActor);
            BlackboardComponent->SetValueAsBool("ChasePlayer", true);
            BlackboardComponent->SetValueAsVector("PlayerPosition", TargetPlayer->GetActorLocation());
        }
        else
        {
            TargetPlayer = nullptr;
            BlackboardComponent->ClearValue("ChasePlayer");
        }
    }
}

void AEnemyAIController::GenerateNewRandomLocation()
{
    if (NavigationSystem)
    {
        FNavLocation ReturnLocation;

        AEnemyCharacter* Enemy = Cast<AEnemyCharacter>(GetPawn());
        if (Enemy->PatrolFuse)
        {
            NavigationSystem->GetRandomPointInNavigableRadius(Enemy->PatrolFuse->GetActorLocation(), 1200, ReturnLocation);
            BlackboardComponent->SetValueAsVector("PatrolLocation", ReturnLocation.Location);
        }
        else
        {
            NavigationSystem->GetRandomPointInNavigableRadius(Enemy->GetActorLocation(), 2000, ReturnLocation);
            BlackboardComponent->SetValueAsVector("PatrolLocation", ReturnLocation.Location);
        }
    }
}

void AEnemyAIController::UnstunEnemy()
{
    BlackboardComponent->SetValueAsBool("IsStunned", false);
}

void AEnemyAIController::StunEnemy()
{
    BlackboardComponent->SetValueAsBool("IsStunned", true);
}

void AEnemyAIController::DistractEnemy(bool IsDistracted)
{
    BlackboardComponent->SetValueAsBool("IsDistracted", IsDistracted);
}
