// Fill out your copyright notice in the Description page of Project Settings.


#include "FuseDoor.h"

void AFuseDoor::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    
    FVector DoorTarget = BaseMesh->GetComponentLocation();

    if (LinkedFuseBox)
    {
        if (LinkedFuseBox->IsOn())
        {
            DoorTarget.Z = BaseZ + 250.5f;
        }
        else
        {
            DoorTarget.Z = BaseZ;
        }
    }

    BaseMesh->SetRelativeLocation(FMath::Lerp(BaseMesh->GetComponentLocation(), DoorTarget, 0.05f));
}

FString AFuseDoor::ViewAction_Implementation(AActor* OtherActor)
{
    return "Fuse Box Door: Replace the fuse in the fuse box and flick the switch to open me!";
}
