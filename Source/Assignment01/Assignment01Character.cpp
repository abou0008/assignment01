// Copyright Epic Games, Inc. All Rights Reserved.

#include "Assignment01Character.h"

#include <openexr/Deploy/OpenEXR-2.3.0/OpenEXR/include/ImathMath.h>

#include "Assignment01Projectile.h"
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/InputSettings.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "InputBehavior.h"
#include "Interactable.h"
#include "Kismet/GameplayStatics.h"
#include "MotionControllerComponent.h"
#include "XRMotionControllerBase.h" // for FXRMotionControllerBase::RightHandSourceId
#include "Kismet/KismetMaterialLibrary.h"

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

//////////////////////////////////////////////////////////////////////////
// AAssignment01Character

AAssignment01Character::AAssignment01Character()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(-39.56f, 1.75f, 64.f)); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->SetRelativeRotation(FRotator(1.9f, -19.19f, 5.2f));
	Mesh1P->SetRelativeLocation(FVector(-0.5f, -4.4f, -155.7f));

	// Create a gun mesh component
	FP_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FP_Gun"));
	FP_Gun->SetOnlyOwnerSee(true);			// only the owning player will see this mesh
	FP_Gun->bCastDynamicShadow = false;
	FP_Gun->CastShadow = false;
	// FP_Gun->SetupAttachment(Mesh1P, TEXT("GripPoint"));
	FP_Gun->SetupAttachment(RootComponent);

	FP_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("MuzzleLocation"));
	FP_MuzzleLocation->SetupAttachment(FP_Gun);
	FP_MuzzleLocation->SetRelativeLocation(FVector(0.2f, 48.4f, -10.6f));

	// Default offset from the character location for projectiles to spawn
	GunOffset = FVector(100.0f, 0.0f, 10.0f);

	// Note: The ProjectileClass and the skeletal mesh/anim blueprints for Mesh1P, FP_Gun, and VR_Gun 
	// are set in the derived blueprint asset named MyCharacter to avoid direct content references in C++.

	// Create VR Controllers.
	R_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("R_MotionController"));
	R_MotionController->MotionSource = FXRMotionControllerBase::RightHandSourceId;
	R_MotionController->SetupAttachment(RootComponent);
	L_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("L_MotionController"));
	L_MotionController->SetupAttachment(RootComponent);

	// Create a gun and attach it to the right-hand VR controller.
	// Create a gun mesh component
	VR_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("VR_Gun"));
	VR_Gun->SetOnlyOwnerSee(true);			// only the owning player will see this mesh
	VR_Gun->bCastDynamicShadow = false;
	VR_Gun->CastShadow = false;
	VR_Gun->SetupAttachment(R_MotionController);
	VR_Gun->SetRelativeRotation(FRotator(0.0f, -90.0f, 0.0f));

	VR_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("VR_MuzzleLocation"));
	VR_MuzzleLocation->SetupAttachment(VR_Gun);
	VR_MuzzleLocation->SetRelativeLocation(FVector(0.000004, 53.999992, 10.000000));
	VR_MuzzleLocation->SetRelativeRotation(FRotator(0.0f, 90.0f, 0.0f));		// Counteract the rotation of the VR gun model.

	// Uncomment the following line to turn motion controllers on by default:
	//bUsingMotionControllers = true;

	TriggerCapsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Trigger Capsule"));
	TriggerCapsule->InitCapsuleSize(55.0f, 96.0f);
	TriggerCapsule->SetCollisionProfileName(TEXT("Trigger"));
	TriggerCapsule->SetupAttachment(RootComponent);
	
	TriggerCapsule->OnComponentBeginOverlap.AddDynamic(this, &AAssignment01Character::OnOverLapBegin);
	TriggerCapsule->OnComponentEndOverlap.AddDynamic(this, &AAssignment01Character::OnOverlapEnd);

	PostProcessComp = CreateDefaultSubobject<UPostProcessComponent>(TEXT("Post Process"));
	PostProcessComp->bAllowConcurrentTick = false;
	PostProcessComp->bNeverNeedsRenderUpdate = false;
	PostProcessComp->bUnbound = false;
	PostProcessComp->SetupAttachment(RootComponent);
	
	CurrentViewAction = "";
}

void AAssignment01Character::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	MyGameInstance = Cast<UMyGameInstance>(GetGameInstance());
	
	//Attach gun mesh component to Skeleton, doing it here because the skeleton is not yet created in the constructor
	FP_Gun->AttachToComponent(Mesh1P, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));

	// Show or hide the two versions of the gun based on whether or not we're using motion controllers.
	if (bUsingMotionControllers)
	{
		VR_Gun->SetHiddenInGame(false, true);
		Mesh1P->SetHiddenInGame(true, true);
	}
	else
	{
		VR_Gun->SetHiddenInGame(true, true);
		Mesh1P->SetHiddenInGame(false, true);
	}

	if (PPMaterial)
	{
		PPMatInstance = UKismetMaterialLibrary::CreateDynamicMaterialInstance(this, PPMaterial);
		FWeightedBlendable WeightedBlendable;
		WeightedBlendable.Object = PPMatInstance;
		WeightedBlendable.Weight = 1;

		PostProcessComp->Settings.WeightedBlendables.Array.Add(WeightedBlendable);
	}

	if (PPStatic)
	{
		PPStaticInstance = UKismetMaterialLibrary::CreateDynamicMaterialInstance(this, PPStatic);
		FWeightedBlendable WeightedBlendable;
		WeightedBlendable.Object = PPStaticInstance;
		WeightedBlendable.Weight = 1;

		PostProcessComp->Settings.WeightedBlendables.Array.Add(WeightedBlendable);
	}
	
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AEnemyCharacter::StaticClass(), Enemies);
	
	ShowHandItem("None");
}

void AAssignment01Character::Tick(float DeltaTime)
{	
	CallMyTrace(true);

	if(Health > 0)
		Health -= HealthDrainPerSecond * DeltaTime;
	else if(MyGameInstance->GetGameState() == Playing)
		MyGameInstance->SetGameState(Dead);

	AActor* ClosestEnemy = nullptr;
	float ClosestDistance = 100000;
	for (AActor* enemy : Enemies)
	{
		float Distance = FVector::Dist(GetActorLocation(), enemy->GetActorLocation());
		if (ClosestEnemy == nullptr || Distance < ClosestDistance)
		{
			ClosestEnemy = enemy;
			ClosestDistance = Distance;
		}
	}

	if (ClosestDistance <= 500)
	{
		PPStaticInstance->SetScalarParameterValue("Opacity", 1.0f * (1 - (ClosestDistance / 500)));
	}
	else
	{
		PPStaticInstance->SetScalarParameterValue("Opacity", 0);
	}
		
	PPMatInstance->SetScalarParameterValue("Opacity", 1 - (Health/100));
}

//////////////////////////////////////////////////////////////////////////
// Input

void AAssignment01Character::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	check(PlayerInputComponent);

	// Bind jump events
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	// Bind fire event
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AAssignment01Character::OnFire);

	// Bind interact event
	PlayerInputComponent->BindAction("Interact", IE_Pressed, this, &AAssignment01Character::Interact);

	// Bind pause game event
	PlayerInputComponent->BindAction("PauseGame", IE_Pressed, this, &AAssignment01Character::PauseGame);

	// Enable touchscreen input
	//EnableTouchscreenMovement(PlayerInputComponent);

	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &AAssignment01Character::OnResetVR);

	// Bind movement events
	PlayerInputComponent->BindAxis("MoveForward", this, &AAssignment01Character::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AAssignment01Character::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AAssignment01Character::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AAssignment01Character::LookUpAtRate);
}

void AAssignment01Character::OnFire()
{
	try
	{
		if (HandItem)
		{
			IInteractable::Execute_Interact(HandItem, this);
		}
	}
	catch (int e)
	{
		UE_LOG(LogTemp, Error, TEXT("FAILED WITH ERROR CODE: %d"), e);		
	}
	
	//CallMyTrace(false);
	//// try and fire a projectile
	//if (ProjectileClass != NULL)
	//{
	//	UWorld* const World = GetWorld();
	//	if (World != NULL)
	//	{
	//		if (bUsingMotionControllers)
	//		{
	//			const FRotator SpawnRotation = VR_MuzzleLocation->GetComponentRotation();
	//			const FVector SpawnLocation = VR_MuzzleLocation->GetComponentLocation();
	//			World->SpawnActor<AAssignment01Projectile>(ProjectileClass, SpawnLocation, SpawnRotation);
	//		}
	//		else
	//		{
	//			const FRotator SpawnRotation = GetControlRotation();
	//			// MuzzleOffset is in camera space, so transform it to world space before offsetting from the character location to find the final muzzle position
	//			const FVector SpawnLocation = ((FP_MuzzleLocation != nullptr) ? FP_MuzzleLocation->GetComponentLocation() : GetActorLocation()) + SpawnRotation.RotateVector(GunOffset);
	//
	//			//Set Spawn Collision Handling Override
	//			FActorSpawnParameters ActorSpawnParams;
	//			ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;
	//
	//			// spawn the projectile at the muzzle
	//			World->SpawnActor<AAssignment01Projectile>(ProjectileClass, SpawnLocation, SpawnRotation, ActorSpawnParams);
	//		}
	//	}
	//}

	// try and play the sound if specified
	//if (FireSound != NULL)
	//{
	//	UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
	//}

	// try and play a firing animation if specified
	//if (FireAnimation != NULL)
	//{
		// Get the animation object for the arms mesh
	//	UAnimInstance* AnimInstance = Mesh1P->GetAnimInstance();
	//	if (AnimInstance != NULL)
	//	{
	//		AnimInstance->Montage_Play(FireAnimation, 1.f);
	//	}
	//}
}

void AAssignment01Character::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

/*void AAssignment01Character::BeginTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == true)
	{
		return;
	}
	if ((FingerIndex == TouchItem.FingerIndex) && (TouchItem.bMoved == false))
	{
		OnFire();
	}
	TouchItem.bIsPressed = true;
	TouchItem.FingerIndex = FingerIndex;
	TouchItem.Location = Location;
	TouchItem.bMoved = false;
}

void AAssignment01Character::EndTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == false)
	{
		return;
	}
	TouchItem.bIsPressed = false;
}*/

void AAssignment01Character::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void AAssignment01Character::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void AAssignment01Character::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AAssignment01Character::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

/*bool AAssignment01Character::EnableTouchscreenMovement(class UInputComponent* PlayerInputComponent)
{
	if (FPlatformMisc::SupportsTouchInput() || GetDefault<UInputSettings>()->bUseMouseForTouch)
	{
		PlayerInputComponent->BindTouch(EInputEvent::IE_Pressed, this, &AAssignment01Character::BeginTouch);
		PlayerInputComponent->BindTouch(EInputEvent::IE_Released, this, &AAssignment01Character::EndTouch);

		//Commenting this out to be more consistent with FPS BP template.
		//PlayerInputComponent->BindTouch(EInputEvent::IE_Repeat, this, &AAssignment01Character::TouchUpdate);
		return true;
	}
	
	return false;
}*/

//int AAssignment01Character::GetGameState()
//{
// 	return GameState;
//}

//void AAssignment01Character::SetGameState(int NewGameState)
//{
//	GameState = (GameStates) NewGameState;
//	OnGameStateChange.Broadcast(GameState);
//}

FString AAssignment01Character::GetCurrentViewAction()
{
	return CurrentViewAction;
}

void AAssignment01Character::DropItem()
{
	HandItem = nullptr;
	
	ShowHandItem("None");
}

void AAssignment01Character::HoldItem(APickupItem* NewItem, FString CompName)
{
	HandItem = NewItem;

	ShowHandItem(CompName);
}

void AAssignment01Character::ShowHandItem(FString ComponentName)
{
	TArray<USceneComponent*> USceneChildren = GetFirstPersonCameraComponent()->GetAttachChildren();
	for(USceneComponent* comp : USceneChildren)
	{
		FString CN = comp->GetName();
		if (CN == "KeyComp" || CN == "FuseComp" || CN == "StunGrenadeComp" || CN == "DistractionGrenadeComp")
		{
			if(CN == ComponentName)
				comp->SetHiddenInGame(false, true);
			else
				comp->SetHiddenInGame(true, true);
		}
	}
}

float AAssignment01Character::GetCurrentHealth()
{
	return ceil(Health);
}

void AAssignment01Character::OnOverLapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor && (OtherActor != this) && OtherComp)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Overlap Begin"));
		if (OtherActor->GetClass()->ImplementsInterface(UInteractable::StaticClass()))
		{
			IInteractable::Execute_Interact(OtherActor, this);
		}
	}
	if (OtherActor->GetClass()->GetName() == "BP_EndZone_C")
		MyGameInstance->SetGameState(Won);
}

void AAssignment01Character::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor && (OtherActor != this) && OtherComp)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Overlap End"));
	}
}

void AAssignment01Character::AddHealth(float Amount)
{
	Health += Amount;
	if(Health > 100.0f)
		Health = 100.0f;
}

void AAssignment01Character::Interact()
{
	CallMyTrace(false);
}

void AAssignment01Character::PauseGame()
{
	MyGameInstance->PauseGame();
}

//***************************************************************************************************
//** Trace functions - used to detect items we are looking at in the world
//***************************************************************************************************
//***************************************************************************************************

//***************************************************************************************************
//** Trace() - called by our CallMyTrace() function which sets up our parameters and passes them through
//***************************************************************************************************

bool AAssignment01Character::Trace(
	UWorld* World,
	TArray<AActor*>& ActorsToIgnore,
	const FVector& Start,
	const FVector& End,
	FHitResult& HitOut,
	ECollisionChannel CollisionChannel = ECC_Pawn,
	bool ReturnPhysMat = false
) {

	// The World parameter refers to our game world (map/level) 
	// If there is no World, abort
	if (!World)
	{
		return false;
	}

	// Set up our TraceParams object
	FCollisionQueryParams TraceParams(FName(TEXT("My Trace")), true, ActorsToIgnore[0]);

	// Should we simple or complex collision?
	TraceParams.bTraceComplex = true;

	// We don't need Physics materials 
	TraceParams.bReturnPhysicalMaterial = ReturnPhysMat;

	// Add our ActorsToIgnore
	TraceParams.AddIgnoredActors(ActorsToIgnore);

	// When we're debugging it is really useful to see where our trace is in the world
	// We can use World->DebugDrawTraceTag to tell Unreal to draw debug lines for our trace
	// (remove these lines to remove the debug - or better create a debug switch!)
	//const FName TraceTag("MyTraceTag");
	//World->DebugDrawTraceTag = TraceTag;
	//TraceParams.TraceTag = TraceTag;


	// Force clear the HitData which contains our results
	HitOut = FHitResult(ForceInit);

	// Perform our trace
	World->LineTraceSingleByChannel
	(
		HitOut,		//result
		Start,	//start
		End, //end
		CollisionChannel, //collision channel
		TraceParams
	);

	// If we hit an actor, return true
	return (HitOut.GetActor() != NULL);
}

//***************************************************************************************************
//** CallMyTrace() - sets up our parameters and then calls our Trace() function
//***************************************************************************************************

void AAssignment01Character::CallMyTrace(bool MouseOver)
{
	// Get the location of the camera (where we are looking from) and the direction we are looking in
	const FVector Start = FirstPersonCameraComponent->GetComponentLocation();
	const FVector ForwardVector = FirstPersonCameraComponent->GetForwardVector();

	// How for in front of our character do we want our trace to extend?
	// ForwardVector is a unit vector, so we multiply by the desired distance
	const FVector End = Start + ForwardVector * 250;

	// Force clear the HitData which contains our results
	FHitResult HitData(ForceInit);

	// What Actors do we want our trace to Ignore?
	TArray<AActor*> ActorsToIgnore;

	//Ignore the player character - so you don't hit yourself!
	ActorsToIgnore.Add(this);

	// Call our Trace() function with the paramaters we have set up
	// If it Hits anything
	if (Trace(GetWorld(), ActorsToIgnore, Start, End, HitData, ECC_Visibility, false))
	{
		// Process our HitData
		if (HitData.GetActor())
		{
			//UE_LOG(LogClass, Warning, TEXT("This a testing statement. %s"), *HitData.GetActor()->GetName());
			ProcessTraceHit(HitData, MouseOver);

		}
		else
		{
			// The trace did not return an Actor
			// An error has occurred
			// Record a message in the error log

			CurrentViewAction = "";
		}
	}
	else
	{
		// We did not hit an Actor
		//ClearPickupInfo();
		CurrentViewAction = "";
	}

}

//***************************************************************************************************
//** ProcessTraceHit() - process our Trace Hit result
//***************************************************************************************************

void AAssignment01Character::ProcessTraceHit(FHitResult& HitOut, bool MouseOver)
{
	if (HitOut.GetActor()->GetClass()->ImplementsInterface(UInteractable::StaticClass()))
	{
		if(!MouseOver)
			IInteractable::Execute_Interact(HitOut.GetActor(), this);
		else
			CurrentViewAction = IInteractable::Execute_ViewAction(HitOut.GetActor(), this);
	}
	else
	{
		//UE_LOG(LogClass, Warning, TEXT("Actor is NOT Interactable!"));
		//ClearPickupInfo();
		CurrentViewAction = "";
	}
}