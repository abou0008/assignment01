// Fill out your copyright notice in the Description page of Project Settings.


#include "StunGrenadeProjectile.h"
#include "Assignment01Character.h"
#include "EnemyAIController.h"
#include "EnemyCharacter.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"

AStunGrenadeProjectile::AStunGrenadeProjectile()
{
    // Use a sphere as a simple collision representation
    CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionComp"));
    CollisionComp->InitSphereRadius(5.0f);
    CollisionComp->BodyInstance.SetCollisionProfileName("Projectile");
    CollisionComp->OnComponentHit.AddDynamic(this, &AStunGrenadeProjectile::OnHit);		// set up a notification for when this component hits something blocking

    // Players can't walk on it
    CollisionComp->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
    CollisionComp->CanCharacterStepUpOn = ECB_No;
    
    // Set as root component
    RootComponent = CollisionComp;

    GrenadeMesh = CreateDefaultSubobject<USceneComponent>(TEXT("GrenadeMesh"));
    GrenadeMesh->SetupAttachment(RootComponent);

    // Use a ProjectileMovementComponent to govern this projectile's movement
    ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComponent"));
    ProjectileMovement->UpdatedComponent = CollisionComp;
    ProjectileMovement->InitialSpeed = 1500.f;
    ProjectileMovement->MaxSpeed = 1500.f;
    ProjectileMovement->bRotationFollowsVelocity = true;
    ProjectileMovement->bShouldBounce = true;

    // Die after 3 seconds by default
    InitialLifeSpan = 3.0f;
}

void AStunGrenadeProjectile::BeginPlay()
{
    Super::BeginPlay();
    
    Ass01Character = Cast<AAssignment01Character>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
    
    MyCoolFx = LoadObject<UNiagaraSystem>(nullptr,
        TEXT("NiagaraSystem'/Game/NiagaraFx/ExplosionSystem.ExplosionSystem'"), nullptr, LOAD_None, nullptr);

}

void AStunGrenadeProjectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
    // Only add impulse and destroy projectile if we hit a physics
    if ((OtherActor != NULL) && (OtherActor != this) && (OtherComp != NULL) && (OtherActor != Ass01Character))
    {
        TArray<AActor*> Enemies;
        UGameplayStatics::GetAllActorsOfClass(GetWorld(), AEnemyCharacter::StaticClass(), Enemies);

        for (AActor* enemy : Enemies)
        {
            float Distance = FVector::Dist(enemy->GetActorLocation(), this->GetActorLocation());

            if (Distance < 300)
            {
                AEnemyCharacter* EnemyCharacter = Cast<AEnemyCharacter>(enemy);
                AEnemyAIController* AIController = Cast<AEnemyAIController>(EnemyCharacter->GetController());
                AIController->StunEnemy();
            }
        }
        
        UNiagaraComponent* effect = UNiagaraFunctionLibrary::SpawnSystemAtLocation(
            GetWorld(),
            MyCoolFx,
            this->GetActorLocation(),
            FRotator(1),
            FVector(1, 1, 1),
            true,
            true,
            ENCPoolMethod::AutoRelease,
            true
        );
        
        Destroy();
    }
}