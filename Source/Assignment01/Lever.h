// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactable.h"
#include "GameFramework/Actor.h"
#include "Lever.generated.h"

UCLASS()
class ASSIGNMENT01_API ALever : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALever();

	class UStaticMeshComponent* Root;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Interact")
	class UStaticMeshComponent* LeverBase;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Interact")
	class UStaticMeshComponent* LeverArm;

	UPROPERTY(EditAnywhere, Category = "Interact")
	UMaterialInterface* Material;

	UMaterialInstanceDynamic* MatInstance;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Interact")
	bool OnState = false;

	float OnAngle = 40.0f;

	float BaseZ;
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interact")
        void Interact(AActor* OtherActor);

	virtual void Interact_Implementation(AActor* OtherActor) override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interact")
        FString ViewAction(AActor* OtherActor);

	virtual FString ViewAction_Implementation(AActor* OtherActor) override;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	bool IsOn();
	
};
