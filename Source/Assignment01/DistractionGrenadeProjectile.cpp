// Fill out your copyright notice in the Description page of Project Settings.


#include "DistractionGrenadeProjectile.h"
#include "Assignment01Character.h"
#include "EnemyAIController.h"
#include "EnemyCharacter.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"

ADistractionGrenadeProjectile::ADistractionGrenadeProjectile()
{
    PrimaryActorTick.bCanEverTick = true;
    
    // Use a sphere as a simple collision representation
    CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionComp"));
    CollisionComp->InitSphereRadius(5.0f);
    CollisionComp->BodyInstance.SetCollisionProfileName("Projectile");
    CollisionComp->OnComponentHit.AddDynamic(this, &ADistractionGrenadeProjectile::OnHit);		// set up a notification for when this component hits something blocking

    // Players can't walk on it
    CollisionComp->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
    CollisionComp->CanCharacterStepUpOn = ECB_No;
    
    // Set as root component
    RootComponent = CollisionComp;

    GrenadeMesh = CreateDefaultSubobject<USceneComponent>(TEXT("GrenadeMesh"));
    GrenadeMesh->SetupAttachment(RootComponent);

    // Use a ProjectileMovementComponent to govern this projectile's movement
    ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComponent"));
    ProjectileMovement->UpdatedComponent = CollisionComp;
    ProjectileMovement->InitialSpeed = 1500.f;
    ProjectileMovement->MaxSpeed = 1500.f;
    ProjectileMovement->bRotationFollowsVelocity = false;
    ProjectileMovement->bShouldBounce = false;

    // Die after 3 seconds by default
    InitialLifeSpan = Lifeline;
}

void ADistractionGrenadeProjectile::BeginPlay()
{
    Super::BeginPlay();
    
    Ass01Character = Cast<AAssignment01Character>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
    
    UGameplayStatics::GetAllActorsOfClass(GetWorld(), AEnemyCharacter::StaticClass(), Enemies);

    SetActorRotation(FRotator(0, 0, 0));
    
    MyCoolFx = LoadObject<UNiagaraSystem>(nullptr,
        TEXT("NiagaraSystem'/Game/NiagaraFx/ExplosionSystem2.ExplosionSystem2'"), nullptr, LOAD_None, nullptr);

}

void ADistractionGrenadeProjectile::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    Lifeline -= DeltaTime;
    
    if (Lifeline < 0.5)
    {
        UNiagaraComponent* effect = UNiagaraFunctionLibrary::SpawnSystemAtLocation(
            GetWorld(),
            MyCoolFx,
            this->GetActorLocation(),
            FRotator(1),
            FVector(1),
            true,
            true,
            ENCPoolMethod::AutoRelease,
            true
        );

        for (AActor* enemy : Enemies)
        {
            AEnemyCharacter* EnemyCharacter = Cast<AEnemyCharacter>(enemy);
            AEnemyAIController* AIController = Cast<AEnemyAIController>(EnemyCharacter->GetController());
            AIController->DistractEnemy(false);
        }
        
        Destroy();
    }
    else
    {
        for (AActor* enemy : Enemies)
        {
            float Distance = FVector::Dist(enemy->GetActorLocation(), this->GetActorLocation());

            if (Distance < 1200)
            {
                AEnemyCharacter* EnemyCharacter = Cast<AEnemyCharacter>(enemy);
                AEnemyAIController* AIController = Cast<AEnemyAIController>(EnemyCharacter->GetController());

                AIController->BlackboardComponent->SetValueAsVector("DistractPosition", GetActorLocation());
                
                if (!AIController->BlackboardComponent->GetValueAsBool("IsDistracted"))
                {
                    AIController->DistractEnemy(true);
                }
            }
        }
    }
}

void ADistractionGrenadeProjectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
    
}