// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "Assignment01HUD.generated.h"

UCLASS()
class AAssignment01HUD : public AHUD
{
	GENERATED_BODY()

public:
	AAssignment01HUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

