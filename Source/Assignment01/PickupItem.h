// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactable.h"
#include "GameFramework/Actor.h"
#include "PickupItem.generated.h"

UCLASS()
class ASSIGNMENT01_API APickupItem : public AActor, public IInteractable
{
	GENERATED_BODY()

protected:
	UPROPERTY(VisibleAnywhere, Category = "Interact")
	FString InspectMessage;
	
	UPROPERTY(VisibleAnywhere, Category = "Interact")
	int SpinSpeed = 80;

	UPROPERTY(VisibleAnywhere, Category = "Interact")
	int BobAmount = 15;

	FVector StartingLocation;
	
public:	
	// Sets default values for this actor's properties
	APickupItem();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Interact")
	class UStaticMeshComponent* BaseMesh;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
