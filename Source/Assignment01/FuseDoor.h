// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FuseBox.h"
#include "LeverDoor.h"
#include "FuseDoor.generated.h"

/**
 * 
 */
UCLASS()
class ASSIGNMENT01_API AFuseDoor : public ALeverDoor
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditAnywhere, Category = "Interact")
	AFuseBox* LinkedFuseBox;

public:
	virtual void Tick(float DeltaTime) override;
	
	virtual FString ViewAction_Implementation(AActor* OtherActor) override;
	
};
