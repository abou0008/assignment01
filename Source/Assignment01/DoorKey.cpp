// Fill out your copyright notice in the Description page of Project Settings.

#include "DoorKey.h"
#include "Assignment01Character.h"
#include "Kismet/GameplayStatics.h"

void ADoorKey::BeginPlay()
{
	Super::BeginPlay();
	InspectMessage = "A Key: Unlocks a locked door";

	Ass01Character = Cast<AAssignment01Character>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
}

void ADoorKey::Interact_Implementation(AActor* OtherActor)
{
	if(OtherActor->GetClass()->StaticClass() == Ass01Character->GetClass()->StaticClass())
	{
		if(Ass01Character->HandItem == nullptr)
		{
			Ass01Character->HoldItem(this, "KeyComp");
			SetActorHiddenInGame(true);
			SetActorEnableCollision(false);
		}
	}
}

FString ADoorKey::ViewAction_Implementation(AActor* OtherActor)
{
	if(OtherActor->GetClass()->StaticClass() == Ass01Character->GetClass()->StaticClass())
	{
		if(Ass01Character->HandItem != nullptr)
			InspectMessage = "You are already holding an item!";
		else
			InspectMessage = "A Key: Unlocks a locked door";
	}
	
	return InspectMessage;
}