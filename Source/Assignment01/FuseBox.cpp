// Fill out your copyright notice in the Description page of Project Settings.


#include "FuseBox.h"
#include "Assignment01Character.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AFuseBox::AFuseBox()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BaseMesh"));
	RootComponent = BaseMesh;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	SceneComponent->SetupAttachment(RootComponent);

	LeverArm = CreateDefaultSubobject<USceneComponent>(TEXT("LeverArm"));
	LeverArm->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AFuseBox::BeginPlay()
{
	Super::BeginPlay();

	TArray<USceneComponent*> USceneChildren = GetRootComponent()->GetAttachChildren();
	for(USceneComponent* comp : USceneChildren)
	{
		if(comp->GetName() == "Fuse")
			comp->SetHiddenInGame(true, true);

		if(comp->GetName() == "LeverArm")
		{
			UStaticMeshComponent* Arm = Cast<UStaticMeshComponent>(comp->GetChildComponent(0));
			Material = Arm->GetMaterial(0);
			MatInstance = Arm->CreateDynamicMaterialInstance(0, Material);
		}
	}
}

// Called every frame
void AFuseBox::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FRotator ArmTarget = LeverArm->GetRelativeRotation();
	FLinearColor LeverColour;
	
	if(OnState)
	{
		ArmTarget.Roll = 0;
		LeverColour = FLinearColor(0.0857, 1.0, 0.0135);
	}
	else
	{
		ArmTarget.Roll = OnAngle;
		LeverColour = FLinearColor(1.0, 0.021, 0.0174);
	}

	LeverArm->SetRelativeRotation(FMath::Lerp(LeverArm->GetRelativeRotation(), ArmTarget, 0.3f));
	MatInstance->SetVectorParameterValue("Colour", FMath::Lerp(MatInstance->K2_GetVectorParameterValue("Colour"), LeverColour, 0.3f));
}

void AFuseBox::Interact_Implementation(AActor* OtherActor)
{
	if(Locked)
	{
		if(OtherActor->GetClass()->StaticClass() == UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)->GetClass()->StaticClass())
		{
			AAssignment01Character* Ass01Character = Cast<AAssignment01Character>(OtherActor);
			if(Ass01Character->HandItem)
			{
				if(LinkedFuse == Ass01Character->HandItem)
				{
					Locked = false;
					Ass01Character->DropItem();

					TArray<USceneComponent*> USceneChildren = GetRootComponent()->GetAttachChildren();
					for(USceneComponent* comp : USceneChildren)
					{
						if(comp->GetName() == "Fuse")
							comp->SetHiddenInGame(false, true);
					}
				}
			}
		}
	}
	else
		if(!OnState)
			OnState = !OnState;
}

FString AFuseBox::ViewAction_Implementation(AActor* OtherActor)
{
	if(Locked)
	{
		if(OtherActor->GetClass()->StaticClass() == UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)->GetClass()->StaticClass())
		{
			AAssignment01Character* Ass01Character = Cast<AAssignment01Character>(OtherActor);
			if(Ass01Character->HandItem)
			{
				if(LinkedFuse == Ass01Character->HandItem)
					return "Fuse Box: Press E to place fuse";
				return "Fuse Box: You cannot place that item in the fuse box!";
			}
		}
		return "Fuse Box: Press E while holding a fuse to place it";
	}
	
	if (!OnState)
		return "Fuse Box: Press E turn on power and open door";
	return "Fuse Box: Power is already on, go find the open door!";
}

bool AFuseBox::IsOn()
{
	return OnState;
}
