// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "LockedDoor.h"
#include "FuseBox.h"
#include "PoweredLockedDoor.generated.h"

/**
 * 
 */
UCLASS()
class ASSIGNMENT01_API APoweredLockedDoor : public ALockedDoor
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, Category = "Interact")
	AFuseBox* LinkedFuseBox;
	
public:
    virtual void Interact_Implementation(AActor* OtherActor) override;
	
    virtual FString ViewAction_Implementation(AActor* OtherActor) override;
};
