// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "NavigationSystem.h"
#include "Assignment01Character.h"
#include "DistractionGrenadeProjectile.h"
#include "Perception/AIPerceptionComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "perception/AISenseConfig_Sight.h"
#include "EnemyAIController.generated.h"

/**
* 
*/
UCLASS()
class ASSIGNMENT01_API AEnemyAIController : public AAIController
{
	GENERATED_BODY()

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
	float SightRadius = 500.f;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
	float SightAge = 3.f;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
	float LoseSightRadius = SightRadius + 30.f;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
	float FieldOfView = 60.f;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
	UAISenseConfig_Sight* SightConfiguration;

	UPROPERTY(EditDefaultsOnly, Category = Blackboard)
	UBlackboardData* AIBlackboard;

	UPROPERTY(EditDefaultsOnly, Category = Blackboard)
	UBehaviorTree* BehaviourTree;

	UPROPERTY(EditDefaultsOnly, Category = Blackboard)
	UBlackboardComponent* BlackboardComponent;
    
	UNavigationSystemV1* NavigationSystem;
	AAssignment01Character* TargetPlayer;
    
public:
	AEnemyAIController();

	virtual void BeginPlay() override;
	virtual void OnPossess(APawn* InPawn) override;
	virtual void Tick(float DeltaSeconds) override;
	virtual FRotator GetControlRotation() const override;
	void OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result) override;

	UFUNCTION()
    void OnSensesUpdated(AActor* UpdatedActor, FAIStimulus Stimulus);
    
	UFUNCTION()
    void GenerateNewRandomLocation();

	UFUNCTION()
    void UnstunEnemy();

	UFUNCTION()
    void StunEnemy();

	UFUNCTION()
    void DistractEnemy(bool IsDistracted);
};
