// Fill out your copyright notice in the Description page of Project Settings.


#include "Door.h"

// Sets default values
ADoor::ADoor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BaseMesh"));
	RootComponent = BaseMesh;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	SceneComponent->SetupAttachment(RootComponent);

	DoorMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("DoorMesh"));
	DoorMesh->SetupAttachment(SceneComponent);
}

// Called when the game starts or when spawned
void ADoor::BeginPlay()
{
	Super::BeginPlay();

	StartingRotation = SceneComponent->GetRelativeRotation().Yaw;

	Material = DoorMesh->GetMaterial(0);
	MatInstance = DoorMesh->CreateDynamicMaterialInstance(0, Material);
}

// Called every frame
void ADoor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FRotator Target = SceneComponent->GetRelativeRotation();
	
	if(Open)
		Target.Yaw = StartingRotation - 100;
	else
		Target.Yaw = StartingRotation;
	
	SceneComponent->SetRelativeRotation(FMath::Lerp(SceneComponent->GetRelativeRotation(), Target, 0.05f));
}

void ADoor::Interact_Implementation(AActor* OtherActor)
{
	Open = !Open;
}

FString ADoor::ViewAction_Implementation(AActor* OtherActor)
{
	return "Door: Press E to open";
}
