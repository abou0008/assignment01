// Fill out your copyright notice in the Description page of Project Settings.


#include "BinaryDoor.h"

void ABinaryDoor::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    
    FVector DoorTarget = BaseMesh->GetComponentLocation();

    if (LinkedLever1 && LinkedLever2 && LinkedLever3)
    {
        if (Combination[0] == LinkedLever1->IsOn()
            && Combination[1] == LinkedLever2->IsOn()
            && Combination[2] == LinkedLever3->IsOn())
        {
            DoorTarget.Z = BaseZ + 255.0f;
        }
        else
        {
            DoorTarget.Z = BaseZ;
        }
    }

    BaseMesh->SetRelativeLocation(FMath::Lerp(BaseMesh->GetComponentLocation(), DoorTarget, 0.05f));
}

FString ABinaryDoor::ViewAction_Implementation(AActor* OtherActor)
{
    return "Binary Door: Pick the right lever combination to open me!";
}