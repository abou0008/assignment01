// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "EnemyCharacter.h"
#include "PickupItem.h"
#include "GameFramework/Character.h"
#include "MyGameInstance.h"
#include "Components/PostProcessComponent.h"
#include "Assignment01Character.generated.h"

class UInputComponent;

UCLASS(config=Game)
class AAssignment01Character : public ACharacter
{
	GENERATED_BODY()

	/** Pawn mesh: 1st person view (arms; seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category=Mesh)
	class USkeletalMeshComponent* Mesh1P;

	/** Gun mesh: 1st person view (seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USkeletalMeshComponent* FP_Gun;

public:
	/** Location on gun mesh where projectiles should spawn. */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USceneComponent* FP_MuzzleLocation;

private:
	/** Gun mesh: VR view (attached to the VR controller directly, no arm, just the actual gun) */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USkeletalMeshComponent* VR_Gun;

	/** Location on VR gun mesh where projectiles should spawn. */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USceneComponent* VR_MuzzleLocation;

	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FirstPersonCameraComponent;

	/** Motion controller (right hand) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UMotionControllerComponent* R_MotionController;

	/** Motion controller (left hand) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UMotionControllerComponent* L_MotionController;

public:
	AAssignment01Character();

protected:
	virtual void BeginPlay() override;

public:	
	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

	/** Gun muzzle's offset from the characters location */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	FVector GunOffset;

	/** Projectile class to spawn */
	UPROPERTY(EditDefaultsOnly, Category=Projectile)
	TSubclassOf<class AAssignment01Projectile> ProjectileClass;

	/** Sound to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	class USoundBase* FireSound;

	/** AnimMontage to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	class UAnimMontage* FireAnimation;

	/** Whether to use motion controller location for aiming. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	uint32 bUsingMotionControllers : 1;

	/** Post process component */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Mesh)
	UPostProcessComponent* PostProcessComp;

	TArray<AActor*> Enemies;

	UPROPERTY(EditAnywhere, Category = "Post Process")
		UMaterialInterface* PPMaterial;

	UMaterialInstanceDynamic* PPMatInstance;

	UPROPERTY(EditAnywhere, Category = "Post Process")
	UMaterialInterface* PPStatic;

	UMaterialInstanceDynamic* PPStaticInstance;

protected:
	virtual void Tick(float DeltaTime) override;

	FString CurrentViewAction;
	
public:
	UPROPERTY(VisibleAnywhere, Category = "Interact")
	APickupItem* HandItem;

	void ShowHandItem(FString ComponentName);
	
	UPROPERTY(VisibleAnywhere, Category = "Interact")
	float Health = 100.0f;

	UPROPERTY(VisibleAnywhere, Category = "Interact")
	float HealthDrainPerSecond = 2.0f;
	
	UPROPERTY(VisibleAnywhere, Category = "Trigger Capsule")
	class UCapsuleComponent* TriggerCapsule;

	UPROPERTY(VisibleAnywhere, Category = "GameState")
	UMyGameInstance* MyGameInstance;

	UFUNCTION(BlueprintCallable, Category = "ViewAction")
	FString GetCurrentViewAction();

	UFUNCTION(BlueprintCallable, Category = "ViewAction")
    void DropItem();
	
	UFUNCTION(BlueprintCallable, Category = "ViewAction")
    void HoldItem(APickupItem* NewItem, FString CompName);

	UFUNCTION(BlueprintCallable, Category = "Health")
    float GetCurrentHealth();
	
	UFUNCTION()
    void OnOverLapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
        UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
        const FHitResult& SweepResult);

	UFUNCTION()
    void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
        UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

protected:
	bool Trace(
	    UWorld* World,
	    TArray<AActor*>& ActorsToIgnore,
	    const FVector& Start,
	    const FVector& End,
	    FHitResult& HitOut,
	    ECollisionChannel CollisionChannel,
	    bool ReturnPhysMat
	);

	void Interact();

	void PauseGame();

	void CallMyTrace(bool MouseOver);

	void ProcessTraceHit(FHitResult &Hitout, bool MouseOver);
		
	/** Fires a projectile. */
	void OnFire();

	/** Resets HMD orientation and position in VR. */
	void OnResetVR();

	/** Handles moving forward/backward */
	void MoveForward(float Val);

	/** Handles stafing movement, left and right */
	void MoveRight(float Val);

	/**
	 * Called via input to turn at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	//struct TouchData
	//{
	//	TouchData() { bIsPressed = false;Location=FVector::ZeroVector;}
	//	bool bIsPressed;
	//	ETouchIndex::Type FingerIndex;
	//	FVector Location;
	//	bool bMoved;
	//};
	//void BeginTouch(const ETouchIndex::Type FingerIndex, const FVector Location);
	//void EndTouch(const ETouchIndex::Type FingerIndex, const FVector Location);
	//void TouchUpdate(const ETouchIndex::Type FingerIndex, const FVector Location);
	//TouchData TouchItem;
	
	// APawn interface
	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;
	// End of APawn interface

	/* 
	 * Configures input for touchscreen devices if there is a valid touch interface for doing so 
	 *
	 * @param	InputComponent	The input component pointer to bind controls to
	 * @returns true if touch controls were enabled.
	 */
	//bool EnableTouchscreenMovement(UInputComponent* InputComponent);

public:
	/** Returns Mesh1P subobject **/
	FORCEINLINE class USkeletalMeshComponent* GetMesh1P() const { return Mesh1P; }
	/** Returns FirstPersonCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetFirstPersonCameraComponent() const { return FirstPersonCameraComponent; }

	void AddHealth(float Amount);
};

