// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Fuse.h"
#include "GameFramework/Actor.h"
#include "FuseBox.generated.h"

UCLASS()
class ASSIGNMENT01_API AFuseBox : public AActor, public IInteractable
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interact")
	class UStaticMeshComponent* BaseMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interact")
	class USceneComponent* SceneComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interact")
	class USceneComponent* LeverArm;
	
	UPROPERTY(EditAnywhere, Category = "Interact")
	AFuse* LinkedFuse;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Interact")
	bool Locked = true;

	UPROPERTY(EditAnywhere, Category = "Interact")
	UMaterialInterface* Material;

	UMaterialInstanceDynamic* MatInstance;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Interact")
	bool OnState = false;

	float OnAngle = 80.0f;
	
public:	
	// Sets default values for this actor's properties
	AFuseBox();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact_Implementation(AActor* OtherActor) override;
	
	virtual FString ViewAction_Implementation(AActor* OtherActor) override;

	bool IsOn();
};
