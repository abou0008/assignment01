// Fill out your copyright notice in the Description page of Project Settings.


#include "PoweredLockedDoor.h"
#include "Assignment01Character.h"
#include "Kismet/GameplayStatics.h"

void APoweredLockedDoor::Interact_Implementation(AActor* OtherActor)
{
    if(Locked)
    {
        if(OtherActor->GetClass()->StaticClass() == UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)->GetClass()->StaticClass())
        {
            AAssignment01Character* Ass01Character = Cast<AAssignment01Character>(OtherActor);
            if(Ass01Character->HandItem)
            {
                if(LinkedKey == Ass01Character->HandItem)
                {
                    Locked = false;
                    Ass01Character->DropItem();
                    if (MatInstance != nullptr)
                        MatInstance->SetScalarParameterValue("Emission", 1.5);
                }
            }
        }
    }
    else if(LinkedFuseBox->IsOn())
    {
        Open = !Open;
    }
}

FString APoweredLockedDoor::ViewAction_Implementation(AActor* OtherActor)
{
    if (!LinkedFuseBox->IsOn() && Locked)
    {
        if(OtherActor->GetClass()->StaticClass() == UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)->GetClass()->StaticClass())
        {
            AAssignment01Character* Ass01Character = Cast<AAssignment01Character>(OtherActor);
            if(Ass01Character->HandItem)
            {
                if(LinkedKey == Ass01Character->HandItem)
                    return "Unpowered Locked Door: Press E to use key. Door still needs power!";
                return "Unpowered Locked Door: That item will not open this door!";
            }
        }
        return "Unpowered Locked Door: Turn on the power and unlock me with a key to open";
    }
    
    if (Locked)
    {
        if(OtherActor->GetClass()->StaticClass() == UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)->GetClass()->StaticClass())
        {
            AAssignment01Character* Ass01Character = Cast<AAssignment01Character>(OtherActor);
            if(Ass01Character->HandItem)
            {
                if(LinkedKey == Ass01Character->HandItem)
                    return "Powered Locked Door: Press E to unlock";
                return "Powered Locked Door: That item will not open this door!";
            }
        }
        return "Powered Locked Door: Press E while holding a key to unlock";
    }
    
    if (!LinkedFuseBox->IsOn())
    {
        return "Unpowered Unlocked Door: Replace the fuse in the fuse box to power the door!";
    }
    
    return "Powered Unlocked Door: Press E to open";
}
