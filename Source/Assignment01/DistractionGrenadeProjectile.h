// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GrenadeProjectile.h"
#include "NiagaraComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraSystem.h"
#include "DistractionGrenadeProjectile.generated.h"

/**
 * 
 */
UCLASS()
class ASSIGNMENT01_API ADistractionGrenadeProjectile : public AGrenadeProjectile
{
	GENERATED_BODY()

public:
	ADistractionGrenadeProjectile();
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
	UNiagaraSystem* MyCoolFx;

	UPROPERTY(EditAnywhere, Category = "Interact")
	float Lifeline = 5.0f;

	TArray<AActor*> Enemies;
	
	/** called when projectile hits something */
	UFUNCTION()
    void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	/** Returns CollisionComp subobject **/
	FORCEINLINE class USphereComponent* GetCollisionComp() const { return CollisionComp; }
	/** Returns ProjectileMovement subobject **/
	FORCEINLINE class UProjectileMovementComponent* GetProjectileMovement() const { return ProjectileMovement; }

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;
};
