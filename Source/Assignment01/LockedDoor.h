// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Door.h"
#include "DoorKey.h"
#include "LockedDoor.generated.h"

/**
 * 
 */
UCLASS()
class ASSIGNMENT01_API ALockedDoor : public ADoor
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditAnywhere, Category = "Interact")
	ADoorKey* LinkedKey;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Interact")
	bool Locked = true;
	
public:
	virtual void Interact_Implementation(AActor* OtherActor) override;
	
	virtual FString ViewAction_Implementation(AActor* OtherActor) override;
};
