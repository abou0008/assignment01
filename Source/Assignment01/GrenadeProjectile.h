// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Assignment01Projectile.h"
#include "GrenadeProjectile.generated.h"

class AAssignment01Character;

/**
 * 
 */
UCLASS()
class ASSIGNMENT01_API AGrenadeProjectile : public AAssignment01Projectile
{
	GENERATED_BODY()

protected:
	AAssignment01Character* Ass01Character;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
	class USceneComponent* GrenadeMesh;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
};
