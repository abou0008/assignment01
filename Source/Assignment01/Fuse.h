// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PickupItem.h"
#include "Fuse.generated.h"

class AAssignment01Character;

/**
 * 
 */
UCLASS()
class ASSIGNMENT01_API AFuse : public APickupItem
{
	GENERATED_BODY()
	
protected:
	AAssignment01Character* Ass01Character;
	
public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interact")
        void Interact(AActor* OtherActor);

	virtual void Interact_Implementation(AActor* OtherActor) override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interact")
        FString ViewAction(AActor* OtherActor);

	virtual FString ViewAction_Implementation(AActor* OtherActor) override;

	virtual void BeginPlay() override;
};
