// Fill out your copyright notice in the Description page of Project Settings.


#include "PickupItem.h"


// Sets default values
APickupItem::APickupItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BaseMesh"));
	BaseMesh->SetCollisionProfileName(TEXT("Trigger"));
	RootComponent = BaseMesh;
}

// Called when the game starts or when spawned
void APickupItem::BeginPlay()
{
	Super::BeginPlay();

	StartingLocation = BaseMesh->GetComponentLocation();
}

// Called every frame
void APickupItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	BaseMesh->SetRelativeLocation(StartingLocation + FVector(0, 0, BobAmount + BobAmount * FMath::Sin(GetGameTimeSinceCreation())));
	BaseMesh->SetRelativeRotation(FRotator(0, BaseMesh->GetRelativeRotation().Yaw + SpinSpeed * DeltaTime, 0));
}

