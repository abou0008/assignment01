// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "MyGameInstance.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnGameStateChange, int, GameState);

enum GameStates
{
	Resetting,
    Restarting,
    StartMenu,
    Playing,
    Paused,
    Dead,
    Won,
	Cinematic
};

/**
 * 
 */
UCLASS()
class ASSIGNMENT01_API UMyGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	UMyGameInstance();

	UFUNCTION(BlueprintCallable, Category = "GameState")
	void SetGameState(int NewGameState);

	UFUNCTION(BlueprintCallable, Category = "GameState")
	int GetGameState();

	UFUNCTION(BlueprintCallable, Category = "GameState")
    void ResetGame();

	UFUNCTION(BlueprintCallable, Category = "GameState")
    void PauseGame();

	UPROPERTY(BlueprintAssignable, VisibleAnywhere, Category = "GameState")
	FOnGameStateChange OnGameStateChange;
	
protected:
	GameStates GameState;
};
