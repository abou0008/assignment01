// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "LeverDoor.h"
#include "BinaryDoor.generated.h"

/**
 * 
 */
UCLASS()
class ASSIGNMENT01_API ABinaryDoor : public ALeverDoor
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditAnywhere, Category = "Interact")
	ALever* LinkedLever1;
	
	UPROPERTY(EditAnywhere, Category = "Interact")
	ALever* LinkedLever2;

	UPROPERTY(EditAnywhere, Category = "Interact")
	ALever* LinkedLever3;

	bool Combination[3] = { true, false, true };

public:
	virtual void Tick(float DeltaTime) override;
	
	virtual FString ViewAction_Implementation(AActor* OtherActor) override;
	
};
