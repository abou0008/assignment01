HOW TO PLAY

Use WASD, Space and the Mouse to move around.

There seems to be a weird effect with the torch where is gets brighter
the longer you are looking at it. If you look at nothing it will reset
and get really dark, so keep looking at a downwards slant to keep the
light even.

Press E to interact with any interactables.

Press P or ESC to open the pause menu whilst in game.

Watch the video for a walk through.


All features should pretty much be working as far as I know.